<?php

define('__DB_HOST', '127.0.0.1');
define('__DB_NAME', 'quiz');
define('__DB_USER', 'root');
define('__DB_PASS', 'quiz_app');

define('BASE_PATH', realpath(dirname(__FILE__)));

//Autoload
function my_autoloader($o_class)
{
    $s_filename = BASE_PATH . '/' . str_replace('\\', '/', $o_class) . '.php';
    include($s_filename);
}
spl_autoload_register('my_autoloader');

//Action config
$a_action_config_data['log'] = [];
$a_action_config_data['result'] = [];
$a_action_config_data['quiz'] = [];