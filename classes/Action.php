<?php

namespace classes;

class Action
{
  public function quiz(){
      $o_user = new User($_POST['username'],$_POST['qzs_id']);
      $o_user->register();
      $o_quiz = new Quiz();
      $o_quiz->initQuiz($_POST['uid'], $_POST['qzs_id']);
      //render to view
      require('view/template.php');

  }
  public function log(){
    $o_quiz = new Quiz();
    $o_quiz->logData(
      $_POST['uid']
      ,$_POST['qus_id']
      ,$_POST['sus_avn_id']
    );
  }
  public function result(){
    $o_quiz = new Quiz();
    print $o_quiz->populateAndShowResult($_POST['uid']);
  }
  public function index(){
    $o_quiz = new Quiz();
    $o_quiz->setQuizOptions();
    //render to view
    require('view/login.php');
  }
}