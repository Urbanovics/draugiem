<?php

namespace classes;

class DB extends \PDO
{
    public $s_sql, $bind_values = [], $i_transactionCounter = 0;
    /**
     * @var \PDOStatement $o_sql;
     */
    private $o_sql;

    public function setBindValue($s_name, $s_value) :DB{
        $this->bind_values[':'.$s_name] = $s_value;
        return $this;
    }
    public function executeSql(){
        try {
            $this->o_sql = $this->prepare($this->s_sql);
            $this->o_sql->execute($this->bind_values);
        } catch (\Exception $e)
        {
            throw new \Exception(__METHOD__ . ' Raised for: ' . var_export($this->s_sql, true) . ' Bind Values: ' . var_export($this->bind_values, true) . ' Error: ' . var_export($this->errorInfo(), true), 0, $e);
        }

    }
    public function __construct($s_sql){
        $s_db_string = "mysql:host=".__DB_HOST.";dbname=".__DB_NAME;
        parent::__construct($s_db_string,__DB_USER,__DB_PASS);
       
        $this->s_sql = $s_sql;
        try
        {
            $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        }
        catch (\PDOException $e)
        {
            die($e->getMessage());
        }
    }
    public function getRowCount() :int
    {
        $this->executeSql();
        return $this->o_sql->rowCount();
    }
    public function getAssoc($b_fetch_one = false)
    {
        $s_style = \PDO::FETCH_ASSOC;
        $this->executeSql();
        return $b_fetch_one ? $this->o_sql->fetch($s_style) : $this->o_sql->fetchAll($s_style);

    }
    public function beginTransaction()
    {
        if(!$this->i_transactionCounter++)
            return parent::beginTransaction();
        return $this->i_transactionCounter >= 0;
    }

    public function commit()
    {
        if(!--$this->i_transactionCounter)
            return parent::commit();
        return $this->i_transactionCounter >= 0;
    }

    public function rollback()
    {
        if($this->i_transactionCounter >= 0)
        {
            $this->i_transactionCounter = 0;
            return parent::rollback();
        }
        $this->i_transactionCounter = 0;
        return false;
    }
}