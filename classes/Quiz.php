<?php
namespace classes;
class Quiz
{
  private $i_uid, $i_qzs_id;
  public $aa_quiz_data_indexed_by_qus = [] , $aa_all_quizs_indexed_by_id;
  private function getQuastions() :array {
    $o_sql = new DB("
      SELECT qus.id, qus.questions_text 
        FROM questions qus 
        WHERE qus.qzs_id = :qzs_id
        ORDER BY rand()
     ");
    return $o_sql->setBindValue('qzs_id', $this->i_qzs_id)
      ->getAssoc();
  }
  private function getQuastionsVariants($i_qus_id) :array {
    $o_sql = new DB("
      SELECT anv.id, anv.variant_text 
        FROM answers_variants anv
        WHERE anv.qus_id = :qus_id 
        ORDER BY rand()  
     ");
    return $o_sql->setBindValue('qus_id', $i_qus_id)
      ->getAssoc();
  }
  private function setQuizData(){
    foreach ($this->getQuastions() AS $aa_quastion_data){
      $s_key = $aa_quastion_data['id'];
      $this->aa_quiz_data_indexed_by_qus[$s_key] = [
        'questions_text' => $aa_quastion_data['questions_text']
        ,'answers_variants' => []
      ];
      foreach ($this->getQuastionsVariants($s_key) AS $aa_answers_variants){
        $this->aa_quiz_data_indexed_by_qus[$s_key]['answers_variants'][] = [
          'id' => $aa_answers_variants['id']
          ,'variant_text' => $aa_answers_variants['variant_text']
        ];
      }
    }
    return $this;
  }
  public function initQuiz($i_uid, $i_qzs_id){
    $this->i_uid = $i_uid;
    $this->i_qzs_id = $i_qzs_id;
    $this->setQuizData();
    return $this;
  }
  public function setQuizOptions(){
    $o_sql = new DB("SELECT qzs.id , qzs.name FROM quizzes qzs");
    foreach ($o_sql->getAssoc() AS $aa_all_quizs){
      $s_key = $aa_all_quizs['id'];
      $this->aa_all_quizs_indexed_by_id[$s_key] = $aa_all_quizs['name'];
    }
  }
  private function validate($i_uid, $i_qus_id){
    $b_allow = false;
    if(!empty($i_uid) && !empty($i_qus_id)){
      $o_sql = new DB("
        SELECT 1 AS is_allow_insert 
          FROM results res 
          WHERE res.uid = :uid
            AND res.qus_count IS NULL 
            AND res.qus_correct_count IS NULL
            AND NOT EXISTS(
              SELECT 1 FROM quiz_log qul
              WHERE qul.uid = :uid
                AND qul.qus_id = :qus_id
          )
      ");
      $a_row = $o_sql->setBindValue('uid',$i_uid)
        ->setBindValue('qus_id', $i_qus_id)
        ->getAssoc(true);
      if(!is_null($a_row['is_allow_insert'])){
        $b_allow = true;
      }
    }
    return $b_allow;
  }
  //log each step
  public function logData($i_uid, $i_qus_id, $i_anv_id){
    if($this->validate($i_uid, $i_qus_id)){
      $o_sql = new DB("INSERT INTO quiz_log (qus_id, sus_anv_id, uid)
      values (:qus_id, :sus_anv_id, :uid)
    ");
      $o_sql->setBindValue('qus_id', $i_qus_id)
        ->setBindValue('sus_anv_id', $i_anv_id)
        ->setBindValue('uid',$i_uid);
      $o_sql->executeSql();
      print json_encode(['result' => 1]);
    }
  }
  public function populateAndShowResult($i_uid){
    $o_sql = new DB("
      SELECT
        sus.name AS username
        ,count(*) AS total_count
        ,sum(anv.is_correct) AS correct_count
        FROM quiz_log qul
        JOIN answers_variants anv ON anv.id = qul.sus_anv_id
        JOIN results res ON res.uid = :uid
        JOIN users sus ON sus.id = res.sus_id
        WHERE qul.uid = :uid
    ");
    $aa_result = $o_sql->setBindValue('uid', $i_uid)
    ->getAssoc(true);

    $o_update = new DB("
      UPDATE results 
        SET qus_count = :qus_count
          ,qus_correct_count = :qus_correct_count
        WHERE uid = :uid
    ");
    $o_update->setBindValue('qus_count', $aa_result['total_count'])
      ->setBindValue('qus_correct_count', $aa_result['correct_count'])
      ->setBindValue('uid', $i_uid)
      ->executeSql();

  return json_encode($aa_result);
  }
}