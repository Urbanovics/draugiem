<?php

namespace classes;
class User
{
  private $s_sus_name , $i_qzs_id;
  public $i_sus_id ,$i_uid;
  private function setUserId(){
    $o_sql = new DB("SELECT sus.id FROM users sus WHERE sus.name = :sus_name limit 1");
    $a_data =  $o_sql->setBindValue('sus_name', $this->s_sus_name)->getAssoc(true);
    $this->i_sus_id = $a_data['id'];
    if(is_null($this->i_sus_id)){
      $this->createUser();
    }
  }
  private function createUser(){
    $o_sql = new DB("INSERT INTO users (name) values (:name)");
    $o_sql->setBindValue('name', $this->s_sus_name)
      ->executeSql();
    $this->i_sus_id = $o_sql->lastInsertId();
  }
  public function __construct($s_name, $i_qzs_id){
    $this->s_sus_name = $s_name;
    $this->i_qzs_id = $i_qzs_id;
    }
  public function register(){
    if(!empty($this->s_sus_name) && !empty($this->i_qzs_id) ){
      $this->setUserId();
      $this->generateUid();
      $o_sql = new DB("INSERT INTO results (uid, qzs_id, sus_id) VALUES (:uid, :qzs_id, :sus_id)");
      $o_sql->setBindValue('uid',$this->i_uid)
        ->setBindValue('sus_id',$this->i_sus_id)
        ->setBindValue('qzs_id',$this->i_qzs_id)
        ->executeSql();
    }
  }
  private function generateUid(){
    $this->i_uid = hexdec(uniqid());
  }
}