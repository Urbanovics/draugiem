<?php
include('config.php');

///Router
$a_request = explode('/', trim($_SERVER['PATH_INFO'],'/'));
$o_action = new \classes\Action();

if(count($a_request) == 1){
  $s_action_name = $a_request[0];
  if(array_key_exists($s_action_name ,$a_action_config_data)){
    $o_action->{$s_action_name}();
  } else {
    $o_action->index();
  }
}

