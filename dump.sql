-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: quiz
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answers_variants`
--

DROP TABLE IF EXISTS `answers_variants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `answers_variants` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primārā atslēgā',
  `qus_id` int(11) NOT NULL COMMENT 'Saite ar jautājumiem',
  `variant_text` text NOT NULL COMMENT 'Atbildes teksts',
  `is_correct` int(11) NOT NULL DEFAULT '0' COMMENT 'Pazīmē ka atbilde ir pareiza',
  PRIMARY KEY (`id`),
  UNIQUE KEY `answers_variants_id_uindex` (`id`),
  KEY `anv_qus_fk_i` (`qus_id`),
  CONSTRAINT `anv_qus_fk` FOREIGN KEY (`qus_id`) REFERENCES `questions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Atbildes varianti ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answers_variants`
--

LOCK TABLES `answers_variants` WRITE;
/*!40000 ALTER TABLE `answers_variants` DISABLE KEYS */;
INSERT INTO `answers_variants` (`id`, `qus_id`, `variant_text`, `is_correct`) VALUES (1,1,'2 mēnešus',1),(2,1,'6 mēnešus',0),(3,1,'4 mēnešus',0),(4,1,'1 mēnešus',0),(5,2,'ar rakstveida rīkojumu',1),(6,2,'ar mutisku pavēli',0),(7,2,'darba devējs nevar atstādināt darbinieku no darba',0),(8,2,'tikai pēc savstarpējas vienošanās ar darbinieku',0),(9,3,'darba likums un citi normatīvie akti, kā arī darba koplīgums un darba kārtības noteikumi',1),(10,3,'darba koplīgums;',0),(11,4,'divos eksemplāros, no kuriem viens glabājās pie darbinieka, otrs - pie darba devēja;',1),(12,4,'vienā eksemplārā, kurš glabājās pie darba devēja;',0),(13,4,'trijos eksemplāros, no kuriem viens glabājās pie darbinieka, otrs - pie darba devēja, trešais - darbinieku arodbiedrībā.\r\n',0),(14,5,'kuras ir jaunākas par 18 gadiem;',1),(15,5,'kuras ir jaunākas par 16 gadiem;',0),(16,5,'kuru darba stāžs ir 10 gadi;',0),(17,5,'kuru darba stāžs ir 20 gadi;',0),(18,6,'rakstveida piezīmi vai rājienu.',1),(19,6,'brīdinājumu;\r\n',0),(20,7,'ar darba līgumu;',1),(21,7,'ar vienošanos;',0),(22,7,'nenodibina.',0),(23,7,'ar uzņēmuma iekšējiem noteikumiem;\r\n',0);
/*!40000 ALTER TABLE `answers_variants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primārā atslēgā',
  `qzs_id` int(11) NOT NULL COMMENT 'Saite ar testiem',
  `questions_text` text NOT NULL COMMENT 'Jautājuma teksts',
  PRIMARY KEY (`id`),
  UNIQUE KEY `questions_id_uindex` (`id`),
  KEY `qus_qzs_fk_i` (`qzs_id`),
  CONSTRAINT `qus_qzs_fk` FOREIGN KEY (`qzs_id`) REFERENCES `quizzes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Jautājumi';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
INSERT INTO `questions` (`id`, `qzs_id`, `questions_text`) VALUES (1,1,'Cik ilgu laiku viena gada laikā darba devējs var norīkot darbinieku darba līgumā neparedzēta darba veikšanai dīkstāves gadījumā?'),(2,1,'Kāda veidā darba devējs var atstādināt darbinieku no darba?'),(3,1,'Darba tiesiskās attiecības regulē Latvijas Republikas Satversme, Latvijai saistošās starptautisko tiesību normas, …………………….'),(4,1,'Cikos eksemplāros sagatavo darba līgumu ?'),(5,1,'Kādām personām, noslēdzot darba līgumu, nenosaka pārbaudi, lai noskaidrotu, vai darbinieks atbilst viņam uzticētā darba veikšanai ?'),(6,1,'Par noteiktās darba kārtības vai darba līguma pārkāpšanu darba devējs darbiniekam var izteikt …………… , minot tos apstākļus, kas norāda uz pārkāpuma izdarīšanu.'),(7,1,'Kā nodibina darba devējs un darbinieks savstarpējās darba tiesiskās attiecības ?');
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quiz_log`
--

DROP TABLE IF EXISTS `quiz_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `quiz_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primārā atslēgā',
  `qus_id` int(11) NOT NULL COMMENT 'Saite ar Jautājumiem',
  `sus_anv_id` int(11) DEFAULT NULL COMMENT 'Saite ar jautājumā atbildes variantu, kuru izvelējas sistēmas lietotājs',
  `uid` bigint(20) NOT NULL COMMENT 'ģenerētais testa identifikators',
  PRIMARY KEY (`id`),
  UNIQUE KEY `quiz_log_id_uindex` (`id`),
  KEY `qul_avn_sus_fk_i` (`sus_anv_id`),
  KEY `qul_qus_fk_i` (`qus_id`),
  KEY `qul_uid_i` (`uid`),
  CONSTRAINT `qul_quz_fk` FOREIGN KEY (`qus_id`) REFERENCES `questions` (`id`),
  CONSTRAINT `qul_sus_avn_fk` FOREIGN KEY (`sus_anv_id`) REFERENCES `answers_variants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=465 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Testa žurnals';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quiz_log`
--

LOCK TABLES `quiz_log` WRITE;
/*!40000 ALTER TABLE `quiz_log` DISABLE KEYS */;
INSERT INTO `quiz_log` (`id`, `qus_id`, `sus_anv_id`, `uid`) VALUES (388,5,14,1607955929380081),(389,1,1,1607955929380081),(390,7,20,1607955929380081),(391,4,11,1607955929380081),(392,4,11,1607955929380081),(393,3,10,1607955929380081),(394,2,5,1607955929380081),(395,6,18,1607955929380081),(396,4,12,1607956044413187),(397,1,4,1607956044413187),(398,1,4,1607956044413187),(399,7,22,1607956044413187),(400,2,7,1607956044413187),(401,5,16,1607956044413187),(402,5,16,1607956044413187),(403,6,18,1607956044413187),(404,6,18,1607956044413187),(405,3,9,1607956044413187),(406,7,20,1607956143409952),(407,5,14,1607956143409952),(408,1,1,1607956143409952),(409,4,11,1607956143409952),(410,6,19,1607956143409952),(411,3,9,1607956143409952),(412,2,7,1607956143409952),(413,1,3,1607956258740410),(414,3,9,1607956258740410),(415,3,9,1607956258740410),(416,6,18,1607956258740410),(417,6,18,1607956258740410),(418,5,17,1607956258740410),(419,5,17,1607956258740410),(420,4,12,1607956258740410),(421,4,12,1607956258740410),(422,4,12,1607956258740410),(423,2,6,1607956258740410),(424,2,6,1607956258740410),(425,2,6,1607956335186228),(426,5,15,1607956335186228),(427,1,1,1607956335186228),(428,3,9,1607956335186228),(429,6,18,1607956335186228),(430,4,13,1607956335186228),(431,7,20,1607956335186228),(432,6,18,1607956785629828),(433,2,8,1607956785629828),(434,4,13,1607956785629828),(435,1,2,1607956785629828),(436,5,16,1607956785629828),(437,7,23,1607956785629828),(438,3,9,1607956785629828),(439,4,13,1607956891933750),(440,6,18,1607956891933750),(441,5,16,1607956891933750),(442,3,9,1607956891933750),(443,1,3,1607956891933750),(444,2,5,1607956891933750),(445,2,5,1607957017393959),(446,4,11,1607957017393959),(447,1,1,1607957017393959),(448,7,21,1607957017393959),(449,6,18,1607957017393959),(450,3,9,1607957017393959),(451,5,15,1607957017393959),(452,4,13,1607957133534835),(453,3,9,1607957133534835),(454,1,4,1607957133534835),(455,7,23,1607957133534835),(456,6,18,1607957133534835),(457,2,8,1607957133534835),(458,5,16,1607957353245204),(459,7,21,1607957353245204),(460,2,6,1607957353245204),(461,6,18,1607957353245204),(462,4,11,1607957353245204),(463,1,3,1607957353245204),(464,3,9,1607957353245204);
/*!40000 ALTER TABLE `quiz_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quizzes`
--

DROP TABLE IF EXISTS `quizzes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `quizzes` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primara atslega identifikators',
  `name` varchar(30) DEFAULT NULL COMMENT 'Testa nosaukums',
  PRIMARY KEY (`id`),
  UNIQUE KEY `quizzes_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Saraksts ar testiem';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quizzes`
--

LOCK TABLES `quizzes` WRITE;
/*!40000 ALTER TABLE `quizzes` DISABLE KEYS */;
INSERT INTO `quizzes` (`id`, `name`) VALUES (1,'DARBA LIKUMS');
/*!40000 ALTER TABLE `quizzes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `results`
--

DROP TABLE IF EXISTS `results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `results` (
  `uid` bigint(20) NOT NULL,
  `qzs_id` int(11) DEFAULT NULL,
  `sus_id` int(11) NOT NULL,
  `qus_count` int(11) DEFAULT NULL,
  `qus_correct_count` int(11) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `results_id_uindex` (`uid`),
  KEY `res_sus_fk_i` (`sus_id`),
  KEY `res_qzs_fk_i` (`qzs_id`),
  CONSTRAINT `res_qzs_fk` FOREIGN KEY (`qzs_id`) REFERENCES `quizzes` (`id`),
  CONSTRAINT `res_sus_fk` FOREIGN KEY (`sus_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `results`
--

LOCK TABLES `results` WRITE;
/*!40000 ALTER TABLE `results` DISABLE KEYS */;
/*!40000 ALTER TABLE `results` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primārā atslēgā',
  `name` varchar(100) NOT NULL COMMENT 'Lietotajā vārds',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Sistēmas lietotāji';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-05 14:17:25
