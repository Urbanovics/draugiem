<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <title>jQuiz</title>
    <script src="../jq/jquery-1.6.2.js"></script>

  <style type="text/css">
    body{
      margin: 0px;
      padding: 0px;
      background: none;
      cursor: default;
      font-size: 12px;
      font-family: Arial, Tahoma;
    }
    .questionContainer {
      width: 600px;
      border: 3px double #CFCFCF;
      padding: 3px;
      margin: 10px;
    }
    ul {
      margin: 0px;
      padding: 5px;
    }
    ul li {
      list-style: none;
    }
    a {
      border: 1px solid #000;
      padding: 2px 5px;
      font-weight: bold;
      font-size: 10px;
      background: #FFF;
      cursor: pointer;
    }
    a:hover {
      background: none;
    }
    .btnContainer {
      width: 96%;
      margin: 10px 0px 10px 2%;
    }
    #progressKeeper {
      width: 600px;
      height: 25px;
      border: none;
      margin: 0px 10px;
      padding: 3px;
    }
    .txtStatusBar {
      margin: 5px 10px;
      font-weight: bold;
    }
    #progress {
      background: green;
      width: 0;
      height: 25px;
      -webkit-transition: width 0.3s ease;
    }
    .radius {
      border-radius: 6px;
      -moz-border-radius: 6px;
      -webkit-border-radius: 6px;
      -o-border-radius: 6px;
    }
    #resultKeeper {
      width: 600px;
      margin: 10px;
      padding: 3px;
      border: 3px double #CFCFCF;
    }
    #resultKeeper div {
      line-height: 20px;
    }
    .totalScore {
      font-weight: bold;
    }
    input {
      position: relative;
      top: 2px;
    }
    h1 {
      border-bottom: 1px solid #CCCCCC;
      font-size: 16px;
      height: 22px;
      margin: 10px;
      text-indent: 5px;
    }
    .prev { float: left; }
    .next, .btnShowResult { float: right; }
    .clear { clear: both; }
    .hide { display: none; }
  </style>
  <script type="text/javascript">
      $(function(){
          var jQuiz = {
              init: function(){
                  $('.btnNext').click(function(){

                      var radio_object = $('input[type=radio]:checked:visible');
                      var sus_avn_id = radio_object.attr('id');
                      var qus_id = radio_object.attr('name');
                      var uid = $('#uid').val();
                      if (radio_object.length == 0) {
                          return false;
                      }

                      $.post('log'
                          ,{sus_avn_id: sus_avn_id, qus_id: qus_id, uid: uid}
                      );
                      $(this).parents('.questionContainer').fadeOut(500, function(){
                          $(this).next().fadeIn(500);
                      });
                      var el = $('#progress');
                      el.width(el.width() + 120 + 'px');

                  });
                  $('.btnShowResult').click(function(){
                      var radio_object = $('input[type=radio]:checked:visible');
                      var sus_avn_id = radio_object.attr('id');
                      var qus_id = radio_object.attr('name');
                      var uid = $('#uid').val();

                      $.post('log'
                          ,{sus_avn_id: sus_avn_id, qus_id: qus_id, uid: uid}
                          ,function(log_data) {
                            if(log_data.result == 1){
                                var resultSet = '';
                                $.post(
                                    'result'
                                    ,{uid: uid}
                                    ,function(response_data) {
                                        resultSet = resultSet + '<h1>Paldies , ' + response_data.username + '!</h1><br>'
                                            +'Tu esi atbildēji uz ' + response_data.correct_count + ' no ' + response_data.total_count
                                            + ' jautājumiem.';
                                        $('#resultKeeper').html(resultSet);
                                    }
                                    ,'json'
                                );
                            }
                          }
                          ,'json'
                      );
                      $('#progress').width(300);
                      $('#progressKeeper').hide();
                      $(this).hide();
                      $('#bar').hide();
                      $('#resultKeeper').show();

                  })
              }
          };
          jQuiz.init();
      })


  </script>

</head>
<body>
<input type ='hidden' id = 'uid' value='<?php print $o_user->i_uid ?>'>
<h1>Zināšanu pārbaude</h1>
<?php
$s_html = '';
$b_first = true;

/**
 * @var \classes\Quiz $o_quiz
 */

$i_qus_count = count($o_quiz->aa_quiz_data_indexed_by_qus);
$i_i = 1;
foreach ($o_quiz->aa_quiz_data_indexed_by_qus AS $i_index => $aa_quiz_data){
  if($b_first){
    $s_class_name = 'questionContainer radius';
    $b_first = false;
  } else {
    $s_class_name = 'questionContainer hide radius';
  }
  $s_live_submit = "<a class='btnNext'>Next &gt;&gt;</a>";
  if($i_i == $i_qus_count){
    $s_live_submit = "<a class='btnShowResult'>Finish!</a>";
  }
  $s_html.= "<div class='{$s_class_name}'>";
  $s_html.= "<div class='question'><b> Jautājums ".$i_i." no ".$i_qus_count." : ".$aa_quiz_data['questions_text']."</b></div>";
  $s_html.= "<div class='answers'><ul>";
  foreach ($aa_quiz_data['answers_variants'] AS $aa_question_variants){
    $s_html.="<li><label><input type='radio' name='".$i_index."' id='".$aa_question_variants['id']."' />".$aa_question_variants['variant_text']."</label></li>";
  }
  $s_html.= "</ul></div>";
  $s_html.= "<div class='btnContainer'>
    <div class='next'>
      {$s_live_submit}
    </div>
    <div class='clear'></div>
    </div>
    </div>";
  $i_i++;
}
print $s_html;
?>
<div id ='bar' class="txtStatusBar">Status Bar</div>
<div id="progressKeeper" class="radius">
  <div id="progress"></div>
</div>
<div id="resultKeeper" class="radius hide"></div>
</body>
</html>


