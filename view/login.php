<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Zināšanu pārbaude</title>
    <link rel="stylesheet" href="jq/themes/base/jquery.ui.all.css">
    <script src="jq/jquery-1.6.2.js"></script>
    <script src="jq/external/jquery.bgiframe-2.1.2.js"></script>
    <script src="jq/ui/jquery.ui.core.js"></script>
    <script src="jq/ui/jquery.ui.widget.js"></script>
    <script src="jq/ui/jquery.ui.mouse.js"></script>
    <script src="jq/ui/jquery.ui.button.js"></script>
    <script src="jq/ui/jquery.ui.draggable.js"></script>
    <script src="jq/ui/jquery.ui.position.js"></script>
    <script src="jq/ui/jquery.ui.dialog.js"></script>
    <link rel="stylesheet" href="../jq/demos/demos.css">
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <script>
        $(function() {
            $( "#dialog" ).dialog({  width: 600 });
        });
    </script>
</head>
<body>

<div class="demo">

    <div id="dialog" title="Reģistrācija">
        <form id = 'login_form' action='index.php/quiz' method='post'>
            <table>
                <tr>
                    <td><b>Jūsu Vārds: </b></td><td><input id='username' type ='text' name='username' required='true'></td>
                </tr>
                <tr>
                    <td><b>Tests: </b></td><td>
                <?php
                   /**
                   * @var \classes\Quiz $o_quiz
                   */
                  $s_html_select = "<select name ='qzs_id' required='true'>";
                  foreach ($o_quiz->aa_all_quizs_indexed_by_id AS $i_id => $s_quiz_name){
                    $s_html_select.="<option></option><option value='{$i_id}'>{$s_quiz_name}</option>";
                  }
                  $s_html_select.= '</select>';
                  print $s_html_select;
                 ?>
              </td></tr>
            </table>
            <button class="btn btn-large btn-primary" type="submit">Ienākt</button>
        </form>
        <!-- Sample page content to illustrate the layering of the dialog -->
        <div class="hiddenInViewSource" style="padding:20px;">
        </div><!-- End sample page content -->
    </div><!-- End demo -->
    <div class="demo-description">
    </div><!-- End demo-description -->
</body>
</html>